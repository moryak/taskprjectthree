<?php


namespace App\Controllers;

use RedBeanPHP\R;
use System\View;

class MainController
{

    private $onEdit = false;

    private const STATUS_MESSAGE =
        [
            '0' => 'Отклонен',
            '1' => 'Подтвержден'
        ];

    /**
     * @throws \ErrorException
     */
    public function actionIndex()
    {
        $forms = R::findAll('form', !isset($_SESSION['logged_user']) ? "status = 1" : '');

        return View::render('index',
            [
                'forms' => $forms,
                'onEdit' => $this->onEdit,
                'statusMessage' => self::STATUS_MESSAGE
            ]);
    }

    public function actionEditReview($parameters)
    {
        $forms = R::findAll('form', !isset($_SESSION['logged_user']) ? "status = 1" : '');
        $onEdit = $parameters['id'];

        return View::render('index',
            [
                'forms' => $forms,
                'onEdit' => $onEdit,
                'statusMessage' => self::STATUS_MESSAGE
            ]);
    }

    public function actionUpdate($parameters)
    {

        $editText = trim($_POST['newText']);
        $form = R::load('form', $parameters['id']);
        $form->text = $editText;
        $form->status = 1;
        R::store($form);



        redirect('/');

    }

}