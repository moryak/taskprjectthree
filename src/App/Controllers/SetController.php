<?php

namespace App\Controllers;

use RedBeanPHP\R;
use System\View;
use System\SimpleImage;

/**
 * Class SetController
 * @package App\Controllers
 */
class SetController
{
    /**
     * @throws \ErrorException
     * @throws \RedBeanPHP\RedException\SQL
     */
    public function action()
    {
        $avatarImage = $this->uploadImg();
        $this->storeReview($avatarImage);
        $getData = $this->getReview();

        return View::render('index',
            [
                'forms' => $getData
            ]);
    }

    /**
     * @param string $avatarImage
     * @throws \RedBeanPHP\RedException\SQL
     */
    private function storeReview(string $avatarImage)
    {
        $name = trim($_POST['name']);
        $email = trim($_POST['email']);
        $phone = trim($_POST['phone']);
        $text = trim($_POST['text']);
        $avatar = $avatarImage;

        $dataBase = R::dispense('form');
        $dataBase->name = $name;
        $dataBase->email = $email;
        $dataBase->phone = $phone;
        $dataBase->text = $text;
        $dataBase->avatar = $avatar;

        \R::store($dataBase);
    }

    /**
     * @return array
     */
    private function getReview()
    {
        $forms = R::findAll('form');

        return $forms;

    }

    /**
     * @return string
     */
    private function uploadImg(): string
    {
        $fileName = $_FILES['image']['name'];
        $tmpName = $_FILES['image']['tmp_name'];

        $mimeType = mime_content_type($tmpName);
        if(!in_array($mimeType, array('image/jpeg', 'image/gif', 'image/png'))){
            return false;
        }
        if (!move_uploaded_file($tmpName, __DIR__ . "/../../public/images/$fileName")) {
            return false;
        }
        $resizedImage = new SimpleImage();
        $resizedImage->load(__DIR__ . "/../../public/images/$fileName");
        if($resizedImage->getHeight() > 240 && $resizedImage->getWidth() > 320 ){
            $resizedImage->resize(320,240);
            $resizedImage->save(__DIR__ . "/../../public/images/$fileName");
        }
        return $fileName;
    }

}