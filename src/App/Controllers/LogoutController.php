<?php


namespace App\Controllers;


class LogoutController
{
    public function actionLogout()
    {
        unset($_SESSION['logged_user']);
        header('Location: /',true, 301);
    }
}