<?php

namespace App\Controllers;

use RedBeanPHP\R;
use System\View;

class LoginController
{

    /**
     * @var array
     */
    private $errorMessage = [
        'passwordName' => '',
        'errorName' => ''
    ];

    public function actionLogin()
    {
        View::render('user');
    }

    /**
     * @throws \ErrorException
     */
    public function actionSendData()
    {
        $name = trim($_POST['name']);
        $password = trim($_POST['password']);
        $user = R::findOne('users', 'name =?', [$name]);
        if (!$this->validation($password, $name, $user)) {

            View::render('user',
                [
                    'passwordName' => $this->errorMessage['passwordName'],
                    'errorName' => $this->errorMessage['errorName']
                ]
            );
        }
        $_SESSION['logged_user'] = $user->name;
        redirect('/');
    }


    /**
     * @param string $password
     * @param $user
     * @return bool
     */
    private function validatePassword(string $password, $user): bool
    {
        if (!$password) {
            $this->errorMessage['passwordName'] = 'Пустое поле ввода';

            return false;
        }

        $userPassword = $user->password;
        if ($userPassword !== $password) {
            $this->errorMessage['passwordName'] = 'пароль не совпадает';

            return false;
        }

        return true;
    }

    /**
     * @param string $password
     * @param string $name
     * @param $user
     * @return bool
     */
    private function validation(string $password, string $name, $user): bool
    {
        if (!$user && $user->name != $name) {
            $this->errorMessage['errorName'] = 'Пользователь с таким логином не существует!';
            return false;
        }

        return $this->validatePassword($password, $user);
    }
}