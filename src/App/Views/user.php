<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Sign in</title>
    <!-- Bootstrap core CSS -->
    <link href="/../css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/my.css">
    <!-- Custom styles for this template -->
    <link href="/../css/signin.css" rel="stylesheet">
</head>
<body class="text-center">
<form class="form-signin" method="post" action="SendData">
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <label class="sr-only">Login name</label>
    <input type="text" name="name" class="form-control" placeholder="Login" required autofocus>
    <?php if ($errorName) echo '*' . $errorName ?>
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" class="form-control" placeholder="Password" required>
    <?php if ($passwordName) echo '*' . $passwordName ?>

    <button class="btn btn-lg btn-primary btn-block" type="submit" name="logIn">Sign in</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
</form>
</body>
</html>
