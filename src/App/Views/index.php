<!DOCTYPE html>
<html lang="eu">
<!--
 У администратора должна быть возможность модерирования.
  Т. е. на странице администратора показаны отзывы с миниатюрами картинок и их статусы (принят/отклонен).
  Отзыв становится видимым для всех только после принятия админом.
  Отклоненные отзывы остаются в базе, но не показываются обычным пользователям.
  Изменение картинки администратором не требуется.
 -->
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/my.css">
</head>
<body>
<a href="/login/login" class="btn-primary">Авторизация</a>
<?php if (isset ($_SESSION['logged_user'])) : ?>
    Привет, <?php echo $_SESSION['logged_user']; ?>!<br/>
    <a href="/logout/logout">Выйти</a>
<?php endif; ?>
<?php if ($forms): ?>
    <?php foreach ($forms as $form): ?>
        <div class="container">
            <div class="media-body">
                <?php if ($onEdit && $onEdit === $form['id']): ?>
                    <h3 class="font-weight-bolder">
                        <b><?= $form['name'] ?>  <?= $form['email'] ?> <?= $form['date'] ?></b>
                    </h3>
                    <?php if (isset($_SESSION['logged_user']) && $form['status'] === 1): ?>
                        <p><?= $statusMessage[$form['status']] ?></p>
                    <?php else: ?>
                        <a href="/main/editReview?id=<?= $form['id'] ?>"><?= $statusMessage[$form['status']] ?></a>
                    <?php endif; ?>
                    <form action="/main/update?id=<?= $form['id'] ?>" method="POST">
                        <textarea name="newText" id="<?= $form['id'] ?>" cols="30"
                                  rows="10"><?= $form['text'] ?></textarea>
                        <button>Отправить</button>
                    </form>
                    <img class="d-flex mr-3" src="/images/<?= $form['avatar'] ?>" alt="Generic placeholder image">
                <?php else: ?>
                    <h3 class="font-weight-bolder">
                        <b><?= $form['name'] ?>  <?= $form['email'] ?> <?= $form['date'] ?></b>
                    </h3>
                    <?php if (isset($_SESSION['logged_user']) && $form['status'] === 1): ?>
                        <p><?= $statusMessage[$form['status']] ?></p>
                    <?php else: ?>
                        <a href="/main/editReview?id=<?= $form['id'] ?>"><?= $statusMessage[$form['status']] ?></a>
                    <?php endif; ?>
                    <?= $form['text'] ?>
                    <img class="d-flex mr-3" src="/images/<?= $form['avatar'] ?>" alt="Generic placeholder image">
                <?php endif; ?>
            </div>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<br>
<form action="Set" name="Set" enctype="multipart/form-data" method="post" id="myForm">
    <div class="form-group">
        <label for="formControlInput1">You Name</label>
        <input type="text" class="form-control" name="name" placeholder="Имя">
    </div>

    <div class="form-group">
        <label for="formControlInput2">Email address</label>
        <input type="email" class="form-control" id="formControlInput2" name="email" placeholder="name@example.com">
    </div>

    <div class="form-group">
        <label for="formControlInput3">You Phone</label>
        <input type="tel" class="form-control" name="phone" pattern="^8\d{3}\d{7}$" value="8" maxlength="11">
    </div>

    <div class="form-group">
        <label for="formControlTextarea">Text Message</label>
        <textarea class="form-control" name="text" id="formControlTextarea" rows="3"></textarea>
    </div>
    <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
    <input type="submit" class="form" value="Отправить">
</form>

<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/my.js"></script>

</body>
</html>