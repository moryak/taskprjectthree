<?php

use System\App;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../System/config_db.php';
require_once __DIR__ . '/../System/functions.php';

App::run();
