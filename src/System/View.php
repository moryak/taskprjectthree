<?php

namespace System;

use System\App;

class View
{
    /**
     * @param string $urlPath
     * @param array $data
     * @return false|string
     * @throws \ErrorException
     */
    public static function render(string $urlPath, array $data = [])
    {
        // Получаем путь, где лежат все представления
        $fileWay = __DIR__ . '/../App/Views/' . $urlPath . '.php';

        // Если представление не было найдено, выбрасываем исключение
        if(!file_exists($fileWay)){
            throw new \ErrorException('View cannot be found');
        }

        // Если данные были переданы, то из элементов массива
        // создаются переменные, которые будут доступны в представлении
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $$key = $value;
            }
        }
        // Отображаем представление
        ob_start();
        include($fileWay);
        $view = ob_get_contents();
        ob_end_clean();

        echo $view;
    }
}